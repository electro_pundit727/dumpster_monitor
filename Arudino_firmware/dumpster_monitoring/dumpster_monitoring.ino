#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

/* set up NODEMCU pins for HC-SR04 trig and echo signals */
#define trigPin 12   // D6 pin of NODEMCU
#define echoPin 13   // D7 pin of NODEMCU
#define wifi_led 5   // D1 pin of NODEMCU. This LED indicates the connecting status between NODEMCU and wifi network.

/* Set these to your desired credentials */
const char* ssid = "your network ssid";                          // type the SSID of your wifi network router
const char* password = "your network password";                  // type the password of your wifi network router
const char* host = "192.168.1.135:5000";                         // web server address to post sensor data
const char* sensor_name = "HC-SR04 UltraSonic sensor";           // sensor name

/* Global variable */
long duration;
int distance;
const char* state = "";

void setup_wifi() {
  WiFi.mode(WIFI_STA);    // set up esp8266 as the station mode

  int attempt = 0;
  delay(10);              // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    if (attempt < 30)
      attempt ++;
    else
      ESP.restart();
    delay(500);
    Serial.print(".");
    digitalWrite(wifi_led, !digitalRead(wifi_led));
  }

  if (WiFi.status() == WL_CONNECTED) {
    digitalWrite(wifi_led, HIGH);
    Serial.println("");
    Serial.println("WiFi connected.");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
}

void setup() {
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(wifi_led, OUTPUT);
  digitalWrite(wifi_led, LOW);
  Serial.begin(115200);

  setup_wifi();
}

void get_sensorData() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1;
  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.println("cm");

  if (distance < 20) {
    state = "Full";
  }
  else {
    state = "Empty";
  }

  Serial.print("State: ");
  Serial.println(state);
  Serial.println("****************************");
  delay(1000);
}
void reconnecting() {
  Serial.println("Now is trying to reconnect...");

  // Loop until we're reconnected
  int attempt = 0;
  while (WiFi.status() != WL_CONNECTED) {
    if (attempt < 30)
      attempt ++;
    else
      ESP.restart();
    delay(500);
    Serial.print(".");
    digitalWrite(wifi_led, !digitalRead(wifi_led));
  }

  digitalWrite(wifi_led, HIGH);
  Serial.println("");
  Serial.println("WiFi reconnected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  //Check WiFi connection status
  if (WiFi.status() == WL_CONNECTED) {
    // read the data from HC-SR04 module
    get_sensorData();

    // convert the sensor data as JSON format
    StaticJsonBuffer<300> JSONbuffer;   // declaring static JSON buffer
    JsonObject& JSONencoder = JSONbuffer.createObject();

    JSONencoder["sensorType"] = sensor_name;
    JSONencoder["height"] = distance;
    JSONencoder["state"] = state;

    char JSONmessageBuffer[300];
    JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
    Serial.println(JSONmessageBuffer);

    // send the sensor data to the web sever
    HTTPClient http;    // declare object of class HTTPClient
    http.begin("http://192.168.1.135:5000/monitor");          // Specify request destination. You should type the address of the server to post sensor data
    http.addHeader("Content-Type", "application/json");       // Specify content-type header

    int httpCode = http.POST(JSONmessageBuffer);             // Send the request
    String payload = http.getString();                       // Get the response payload                           

    if (httpCode == HTTP_CODE_OK) {      
      Serial.println(httpCode);   //Print HTTP return code
      Serial.println("The Sensor data is posted to the sever successfully.");
    }
    else {
      Serial.println(httpCode);
      Serial.println("The posting to the server failed!");
      Serial.println(payload);    //Print request response payload
    }      
   
    http.end();  //Close connection
  }
  else {
    Serial.println("Error in WiFi connection.");
    reconnecting();
  }
  delay(5000);    // send a post request every 5 seconds
}

