from flask import Flask, request

app = Flask(__name__)


@app.route('/hello')
def hello_world():
    return 'Hello World!'


@app.route('/monitor', methods=['POST'])
def display_dumpster_state():
    req_data = request.get_json()

    sensorType = req_data['sensorType']
    height = req_data['height']
    state = req_data['state']

    return '''
    The sensor is: {}
    The current height of the dumpster is: {}
    The current state of the dumpster is: {}
    '''.format(sensorType, height, state)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
